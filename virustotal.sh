#!/bin/bash
#-------------------------------------------------------
dep_check() {
which curl > /dev/null 2> /dev/null
if [[ "$?" != 0 ]] ; then
 echo "You are missing curl, which is required to run this script. Please install curl and try again."
 exit 9
fi
which jq > /dev/null 2> /dev/null
if [[ "$?" != 0 ]] ; then
 echo "You are missing jq, which is required to run this script. Please install jq and try again."
 exit 9
fi
}
#-------------------------------------------------------
file_check() {
if [[ ! -d ~/vtconf ]] ; then
 mkdir ~/vtconf
 touch ~/vtconf/api_key
 echo "Before using this program, specify your api-key." 
 echo "Use '$program -s <your api-key>' to set it."
 exit 0
fi
if [[ ! -f ~/vtconf/api_key ]] ; then
 touch ~/vtconf/api_key
 echo "Before using this program, specify your api-key." 
 echo "Use '$program -s <your api-key>' to set it."
 exit 0
fi
}
#------------------------------------------------------
key_check() {
 if [[ "$api_key" == "" ]] ; then
  echo "No api-key is specified. You must specify your api key before scanning a file or getting a report from a file."
  echo "Use '$program -s <your api-key>' to set it."
  echo "Use '$program -h' if you need help."
  exit 8
 fi
}
#------------------------------------------------------
param_check() {
if [[ $# == 0 ]] ; then
 echo "No parameters set."
 echo "Use '$program -h' if you need help."
 exit 1
fi

while getopts "s:f:i:gruah" opt; do
 case ${opt} in
  s )
   echo "Saving your api-key..."
   echo "api_key=$OPTARG" > ~/vtconf/api_key
   ;;
  g )
   key_check
   echo "Your api-key is: ${api_key}"
   exit 0
   ;;
  f )
   key_check
   file=$OPTARG
   if [[ ! -f $OPTARG ]] ; then
   echo "The file doesn't exist!"
   exit 5
   fi
   ;;
  u )
   key_check
   option="scan"
   ;;
  r )
   key_check
   option="report"
   ;;
  h )
   help_page
   ;;
  a )
   filter="general,dates,names,votes,results"
   ;;
  i )
   filter=$OPTARG
   ;;
  \? )
   echo "Invlalid option: $OPTARG" 1>&2
   echo "Use '$program -h' if you need help."
   exit 2
   ;;
  : )
   echo "Invalid option: $OPTARG requires an argument"
   echo "Use '$program -h' if you need help."
   exit 3
 esac
done
shift $((OPTIND -1))

}
#------------------------------------------------------
report() {
sha256=$(sha256sum $file | cut -d ' ' -f 1)
url="https://www.virustotal.com/api/v3/files/${sha256}"
header="\"x-apikey: ${api_key}\""
not_found=0
result=$(curl --request GET --url https://www.virustotal.com/api/v3/files/${sha256} --header "x-apikey: ${api_key}" --silent)

if [[ "$(echo $result | jq .error.code | tr -d "\"")" == "NotFoundError" ]] ; then
 echo "File was not found in the database."
 echo "Possible reasons:"
 echo "-The file was never uploaded."
 echo "-The file analysis just started."
elif [[ "$(echo $result | jq .data.attributes)" == "null" || "$(echo $result | jq .data.attributes)" == "" ]] ; then
 echo "Something went wrong while geting the results."
 echo "Posibel reasons:"
 echo "-The analysis just finished on the file."
 exit 7
else
 echo $result | jq > ~/vtconf/last_result
 echo $file > ~/vtconf/last_file
 echo "Result saved in '~/vtconf/last_result'."
fi
}
#------------------------------------------------------
submit_file() {
size=$(($(ls $file -s | cut -d " " -f 1) / 1024))
if [[ $size -ge 32 ]] ; then
 result=$(curl --request GET --url https://www.virustotal.com/api/v3/files/upload_url --header "x-apikey: ${api_key}" --silent)
 if [[ $? != 0 ]] ; then
  echo "Something went wrong while reaching virustotal.com."
  exit 6
 fi
 url=$(echo $result | jq .data | tr -d "\"")
 echo "Uploading large file."
 curl --request POST --url $url --header "x-apikey: ${api_key}" --form file=@$file  --output /dev/null
else
 echo "Uploading file."
 curl --request POST --url https://www.virustotal.com/api/v3/files --header "x-apikey: ${api_key}" --form file=@$file --output /dev/null
fi
if [[ $? != 0 ]] ; then
 echo "Something went wrong while reaching virustotal.com."
 exit 6
fi
echo $file > ~/vtconf/last_file
}
#------------------------------------------------------
file_set_check() {
if [[ "$file" == "" ]] ; then
 echo "No file specified."
 exit 4
fi
}
#------------------------------------------------------
help_page() {
cat << END
Usage:
$program [options]

 option		meaning
 -s <api-key>	Sets your api key to ~/vtconf/api_key.
 -g		Writes out your api-key.
 -f <file>	Sets the file.
 -u		Uploads the selected file for scan..
 -r		Saves the result of your file in "~/vtconf/last_result"
 -a		Shows all information about the file.
 -i <filter>	Shows the given information about the file.
		Filter can be: "general", "names", "dates", "votes", "result".
		You can use multiple filters by separating them with ",".
 -h		Shows this page.

END
}
#------------------------------------------------------
dep_check
program=$(basename $0)
file_check
. ~/vtconf/api_key
param_check $@

if [[ $option == "report" ]] ; then
 file_set_check
 report
 exit 0
elif [[ $option == "scan" ]] ; then
 file_set_check
 submit_file
 exit 0
fi

if [[ "$filter" != "" ]] ; then
 if [[ ! -f ~/vtconf/last_result ]] ; then
  echo "No last result found. Use \"$program -h\" if you need help."
 elif [[ "$(cat ~/vtconf/last_result)" == "" || "$(cat ~/vtconf/last_result | jq .data.attributes | tr -d "\"")" == "null" ]] ; then
  echo "The file containing the last results is empty or unreadable."
  exit 2
 else
  echo "Showing information about '$(cat ~/vtconf/last_file)'"
  echo
  for fOpt in $(echo $filter | tr "," " ") ; do
   if [[ "$fOpt" != "" ]] ; then
    result=$(cat ~/vtconf/last_result)
    if [[ $fOpt == names ]] ; then
     echo "Names:"
     cat ~/vtconf/last_result | jq .data.attributes.names | tr -d "[]\"\n " | tr "," "\n"
     echo -e "\n"
    elif [[ $fOpt == "dates" ]] ; then
     echo -n "Creation date: "
     date -d @$(echo $result | jq .data.attributes.creation_date)
     echo -n "Last modification date: "
     date -d @$(echo $result | jq .data.attributes.last_modification_date)
     echo -n "Last analysis date: "
     lad=$(echo $result | jq .data.attributes.last_analysis_date)
     if [[ "$lad" == "null" ]] ; then
      echo "no analysis yet"
     else
      date -d @$lad
     fi
     echo -n "First submission date: "
     date -d @$(echo $result | jq .data.attributes.first_submission_date)
     echo -n "Last submission date: "
     date -d @$(echo $result | jq .data.attributes.last_submission_date)
     echo
    elif [[ $fOpt == "general" ]] ; then
     echo "General information:"
     echo -n "Suggested threat label: "
     tl=$(echo $result | jq .data.attributes.popular_threat_classification.suggested_threat_label | tr -d "\"")
     if [[ "$tl" == "null" ]] ; then
      echo "none"
     else
      echo "$tl"
     fi
     echo -n "sources: "
     echo $result | jq .data.attributes.unique_sources
     echo -n "size: "
     echo $result | jq .data.attributes.size
     echo -n "sha1: "
     echo $result | jq .data.attributes.sha1 | tr -d "\""
     echo -n "sha256: "
     echo $result | jq .data.attributes.sha256 | tr -d "\""
     echo -n "md5: "
     echo $result | jq .data.attributes.md5 | tr -d "\""
     echo "Last analysis result:"
     echo $result | jq .data.attributes.last_analysis_stats | tr -d "{}\"\n" | tr "," "\n"
     echo -e "\n"
    elif [[ $fOpt == "votes" ]] ; then
     echo -ne "Reputation: "
     echo $result | jq .data.attributes.reputation
     echo "Votes"
     echo $result | jq .data.attributes.total_votes | tr -d "{}\"\n" | tr "," "\n"
     echo -e "\n"
    elif [[ $fOpt == "results" ]] ; then
     echo "Last scan results:"
     echo $result | jq .data.attributes.last_analysis_results | tr -d "{} \","
     echo
    else
     echo -e "Invalid filter option. \"$fOpt\" is not a valid option.\n"
    fi
   fi
  done
 fi
fi